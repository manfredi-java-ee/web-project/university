<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="resources/css/bootstrap.css" rel="stylesheet"
	type="text/css" />

<title>Detalle de Alumnos</title>
</head>
<body>
	<%@ include file="../fragments/menu.jsp"%>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3>Detalle de Alumnos</h3>
		</div>


		<div class="panel-body">
			<form:form action="guardarAlumno" modelAttribute="alumno"
				cssClass="form-horizontal">
				<spring:bind path="nombre">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:label path="nombre" cssClass="col-sm-2 control-label">Nombre</form:label>
						<div class="col-xs-4">
							<form:input path="nombre" value="${alumno.nombre}"
								cssClass="form-control" />
						</div>
					</div>
				</spring:bind>
				<spring:bind path="apellido">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:label path="apellido" cssClass="col-sm-2 control-label">apellido</form:label>
						<div class="col-xs-4">
							<form:input path="apellido" value="${alumno.apellido}"
								cssClass="form-control" />
						</div>
					</div>
				</spring:bind>

				<div class="col-sm-offset-5">
					<input type="submit" value="Guardar" class="btn btn-primary">
					<a href="volverAlumno" class="btn btn-primary" >Volver</a>
				</div>
			</form:form>
		</div>
	</div>
</body>
</html>