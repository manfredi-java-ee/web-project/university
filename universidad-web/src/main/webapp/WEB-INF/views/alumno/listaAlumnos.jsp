<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="resources/css/bootstrap.css" rel="stylesheet"
	type="text/css" />

<title>Listado de Alumnos</title>
</head>
<body>
	<%@ include file="../fragments/menu.jsp"%>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3>Listado de Alumnos</h3>
		</div>

		<div class="panel-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Id</th>
						<th>Nombre</th>
						<th>Apellido</th>
						<th><a href="detalleAlumno" class="btn btn-primary" >Nuevo <span	class="glyphicon glyphicon-plus-sign"></span></a>	</th>
					</tr>
				</thead>

				<c:forEach items="${alumnos}" var="alumno">
					<tr>
						<td>${alumno.id}</td>
						<td>${alumno.nombre}</td>
						<td>${alumno.apellido}</td>
						<td></td>
					</tr>
				</c:forEach>

			</table>
		</div>
	</div>
</body>
</html>