<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="resources/css/bootstrap.css" rel="stylesheet"
	type="text/css" />

<title>Detalle de Alumnos</title>
</head>
<body>
	<%@ include file="../fragments/menu.jsp"%>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3>Alta de Alumno por Curso</h3>
		</div>

		<div class="panel-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Id</th>
						<th>Nombre</th>
						<th>Apellido</th>
						<th>Agregar</th>
					</tr>
				</thead>

				<c:forEach items="${alumnos}" var="alumno">
					<tr>
						<td>${alumno.id}</td>
						<td>${alumno.nombre}</td>
						<td>${alumno.apellido}</td>
						<td><a href="guardarAlumnoCurso?id=${alumno.id}"><span class="glyphicon glyphicon-pencil"></span></a></td>
					</tr>
				</c:forEach>
			</table>
			<div class="col-sm-offset-5">
				<a href="volverAlumnoCurso" class="btn btn-primary">Volver</a>
			</div>
		</div>
	</div>
</body>
</html>