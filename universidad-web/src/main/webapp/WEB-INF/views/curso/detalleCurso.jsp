<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="resources/css/bootstrap.css" rel="stylesheet"
	type="text/css" />

<title>Detalle del Curso</title>
</head>
<body>

	<%@ include file="../fragments/menu.jsp"%>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3>Detalle del Curso</h3>
		</div>

		<div class="panel-body">
			<form:form action="guardarCurso" modelAttribute="curso"
				cssClass="form-horizontal">
				<spring:bind path="id">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<div class="col-xs-4">
							<form:hidden path="id" value="${curso.id}" />
						</div>
					</div>
				</spring:bind>
				<spring:bind path="nombre">
					<div class="form-group ${status.error ? 'has-error' : ''}">
						<form:label path="nombre" cssClass="col-sm-2 control-label">Nombre</form:label>
						<div class="col-xs-4">
							<form:input path="nombre" value="${curso.nombre}" cssClass="form-control" />
						</div>
					</div>
				</spring:bind>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>Id</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th>Eliminar</th>
							<th><a href="detalleAlumnoCurso" class="btn btn-primary">Agregar Alumno <span class="glyphicon glyphicon-plus-sign"></span></a></th>
						</tr>
					</thead>
					<c:forEach items="${curso.alumnos}" var="alumno">
						<tr>
							<td>${alumno.id}</td>
							<td>${alumno.nombre}</td>
							<td>${alumno.apellido}</td>
							<td><a href="eliminarAlumnosCurso?id=${alumno.id}"><span class="glyphicon glyphicon-remove"></span></a></td>
							<td></td>
						</tr>
					</c:forEach>
				</table>
				<div class="col-sm-offset-5">
					<input type="submit" value="Guardar" class="btn btn-primary">
					<a href="volverCurso" class="btn btn-primary">Volver</a>
				</div>
			</form:form>
		</div>
	</div>
</body>
</html>