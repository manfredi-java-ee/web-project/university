<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="resources/css/bootstrap.css" rel="stylesheet" type="text/css" />

<title>Listado de Cursos</title>
</head>
<body>
	<%@ include file="../fragments/menu.jsp"%>

	<div class="panel panel-default">
		<div class="panel-heading">
			<h3>Listado de Cursos</h3>
		</div>

		<div class="panel-body">
			<table class="table table-hover">
				<thead>
					<tr>
						<th>Id</th>
						<th>Nombre</th>
						<th>Editar</th>
					</tr>
				</thead>

				<c:forEach items="${cursos}" var="curso">
					<tr>
						<td>${curso.id}</td>
						<td>${curso.nombre}</td>
						<td><a href="detalleCurso?id=${curso.id}"><span	class="glyphicon glyphicon-pencil"></span></a></td>
					</tr>
				</c:forEach>

			</table>
		</div>
	</div>
</body>
</html>