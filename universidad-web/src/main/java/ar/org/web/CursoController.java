package ar.org.web;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import ar.org.commons.dto.AlumnoDTO;
import ar.org.commons.dto.CursoDTO;
import ar.org.commons.filter.base.FiltroAlumno;
import ar.org.service.ServiceAlumno;
import ar.org.service.ServiceCurso;

@Controller
public class CursoController {
		
	private final Logger LOG = LoggerFactory.getLogger(CursoController.class);
	@Autowired
	private ServiceCurso serviceCurso;

	@Autowired
	private ServiceAlumno serviceAlumno;
	
	private CursoDTO curso;
	

	@RequestMapping(value = { "listaCursos", "/" })
	public ModelAndView verlistar() {		
		
		List<CursoDTO> cursos = serviceCurso.getAll(null);		
		LOG.debug("listando cursos, candtidad: {}", cursos.size());
		return new ModelAndView("/curso/listaCursos", "cursos", cursos);
	}

	@RequestMapping("detalleCurso")
	public ModelAndView verDetalle(int id) {		
		this.curso = serviceCurso.getById(new Long(id));
		return new ModelAndView("/curso/detalleCurso", "curso", this.curso);
	}

	@RequestMapping("eliminarAlumnosCurso")
	public ModelAndView eliminarAlumno(@RequestParam int id) {
		this.curso.getAlumnos().remove(serviceAlumno.getById(new Long(id)));
		return new ModelAndView("/curso/detalleCurso", "curso", this.curso);
	}

	@RequestMapping(value = "detalleAlumnoCurso")
	public ModelAndView verDetalleAlumnosCursos(Model model) {		
		model.addAttribute("alumno", new AlumnoDTO());		
		model.addAttribute("alumnos",  getAlumnosNoInscriptosCurso());
		return new ModelAndView("/curso/detalleAlumnoCurso");
	}

	@RequestMapping(value = "guardarAlumnoCurso")
	public ModelAndView guardarAlumnosCurso(int id, Model model) {		
		AlumnoDTO alumno = serviceAlumno.getById(new Long(id));		
		if (!this.curso.getAlumnos().contains(alumno)) {
			this.curso.getAlumnos().add(alumno);
		}
		model.addAttribute("alumnos", getAlumnosNoInscriptosCurso());
		return new ModelAndView("/curso/detalleAlumnoCurso");
	}
	
	@RequestMapping(value = "guardarCurso")
	public String guardar(@ModelAttribute CursoDTO curso) {
		curso.setAlumnos(this.curso.getAlumnos());
		serviceCurso.save(curso);
		return "redirect:listaCursos";
	}
	
	@RequestMapping(value = "volverAlumnoCurso")
	public ModelAndView volverAlumnoCurso() {		
		return new ModelAndView("/curso/detalleCurso", "curso", this.curso);
	}
	
	@RequestMapping(value = "volverCurso")
	public String volver() {
		return "redirect:listaCursos";
	}
	
	public List<AlumnoDTO> getAlumnosNoInscriptosCurso(){		
		FiltroAlumno filtro = new FiltroAlumno();
		ArrayList<Long> list = new ArrayList<Long>();
		for (AlumnoDTO alumno : this.curso.getAlumnos()) {
			list. add(alumno.getId());			
		}		
		filtro.setIds(list.toArray(new Long[list.size()]));
		return serviceAlumno.getAlumnos(filtro);
	}
}
