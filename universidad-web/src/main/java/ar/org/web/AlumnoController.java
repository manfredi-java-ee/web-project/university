package ar.org.web;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import ar.org.commons.dto.AlumnoDTO;
import ar.org.service.ServiceAlumno;

@Controller
public class AlumnoController {

	private final Logger LOG = LoggerFactory.getLogger(AlumnoController.class);
	
	@Autowired
	private ServiceAlumno serviceAlumno;

	@RequestMapping(value = { "listaAlumnos", "/" })
	public ModelAndView listar() {
		List<AlumnoDTO> alumnos = serviceAlumno.getAll(null);
		LOG.debug("listando alumnos, cantidad: {}", alumnos.size());
		return new ModelAndView("/alumno/listaAlumnos", "alumnos", alumnos);
	}

	@RequestMapping("detalleAlumno")
	public ModelAndView verDetalle(Model model) {
		model.addAttribute("alumno", new AlumnoDTO());
		return new ModelAndView("/alumno/detalleAlumno");
	}

	@RequestMapping(value = "guardarAlumno")
	public String guardar(@ModelAttribute AlumnoDTO alumno) {
		serviceAlumno.save(alumno);
		return "redirect:listaAlumnos";
	}
	
	@RequestMapping(value = "volverAlumno")
	public String volver(@ModelAttribute AlumnoDTO alumno) {		
		return "redirect:listaAlumnos";
	}
}
