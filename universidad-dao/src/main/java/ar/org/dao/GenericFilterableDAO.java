package ar.org.dao;

import java.io.Serializable;
import java.util.List;

import ar.org.commons.filter.base.FiltroBase;
import ar.org.domain.base.EntityBase;

public interface GenericFilterableDAO<ENTITY extends EntityBase, FILTER extends FiltroBase, ID extends Serializable> extends GenericDAO<ENTITY, ID> {
	
	public abstract List<ENTITY> getAll(FILTER filtro);
	
}