package ar.org.dao;

import java.io.Serializable;
import java.util.List;

import ar.org.domain.base.EntityBase;

public interface GenericDAO<ENTITY extends EntityBase, ID extends Serializable> {
    
	public ENTITY create(ENTITY t);
    
    public ENTITY find(ID id);
    
    public ENTITY update(ENTITY t);
    
    public ENTITY save(ENTITY t);
    
    public void delete(ENTITY t);
    
    public List<ENTITY> getAll();
    
}