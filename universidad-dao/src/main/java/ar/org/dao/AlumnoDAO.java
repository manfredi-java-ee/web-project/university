package ar.org.dao;

import java.util.List;

import ar.org.commons.filter.base.FiltroAlumno;
import ar.org.domain.model.Alumno;

public interface AlumnoDAO extends GenericFilterableDAO<Alumno, FiltroAlumno, Long>{
	
	public List<Alumno> getAlumnos(FiltroAlumno filtro);
}
