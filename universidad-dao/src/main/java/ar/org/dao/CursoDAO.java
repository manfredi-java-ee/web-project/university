package ar.org.dao;

import ar.org.commons.filter.base.FiltroBase;
import ar.org.domain.model.Curso;

public interface CursoDAO extends GenericFilterableDAO<Curso, FiltroBase, Long>{

}
