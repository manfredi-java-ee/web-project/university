package ar.org.domain.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import ar.org.domain.base.EntityBase;

@Entity
@Table(name = "alumno")
public class Alumno extends EntityBase{

	private Long id;
	private String nombre;
	private String apellido;
	private List<Curso> cursos = new ArrayList<Curso>();

	@Id
	@Column(name = "id_alumno")
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "nombre")
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Column(name = "apellido")
	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "alumno_curso", joinColumns = { @JoinColumn(name = "id_alumno") }, inverseJoinColumns = { @JoinColumn(name = "id_curso") })
	public List<Curso> getCursos() {
		return cursos;
	}

	public void setCursos(List<Curso> cursos) {
		this.cursos = cursos;
	}
}
