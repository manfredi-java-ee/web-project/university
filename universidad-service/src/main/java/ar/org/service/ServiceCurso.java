package ar.org.service;

import ar.org.commons.dto.CursoDTO;
import ar.org.commons.filter.base.FiltroBase;
import ar.org.service.base.GenericFilterableCrudService;

public interface ServiceCurso extends GenericFilterableCrudService<CursoDTO, FiltroBase, Long>{

}
