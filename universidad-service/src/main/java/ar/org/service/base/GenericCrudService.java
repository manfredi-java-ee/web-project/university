package ar.org.service.base;

import java.io.Serializable;
import java.util.List;

import ar.org.commons.base.DTOBase;

public interface GenericCrudService<DTO extends DTOBase, ID extends Serializable> extends ServiceBase {
	
	public DTO save(DTO dto);

	public DTO getById(ID id);
	
	public DTO update(DTO dto);

	public void delete(DTO dto);
	
	public List<DTO> getAll();

}
