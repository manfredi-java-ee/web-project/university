package ar.org.service.base;

import java.io.Serializable;
import java.util.List;

import ar.org.commons.base.DTOBase;
import ar.org.commons.filter.base.FiltroBase;

public interface GenericFilterableCrudService <DTO extends DTOBase, FILTER extends FiltroBase, ID extends Serializable> extends GenericCrudService<DTO, ID> {
	
	public abstract List<DTO> getAll(FILTER filtro);
}
