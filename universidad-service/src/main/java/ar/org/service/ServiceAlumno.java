package ar.org.service;

import java.util.List;

import ar.org.commons.dto.AlumnoDTO;
import ar.org.commons.filter.base.FiltroAlumno;
import ar.org.service.base.GenericFilterableCrudService;

public interface ServiceAlumno extends GenericFilterableCrudService<AlumnoDTO, FiltroAlumno, Long>{
	
	
	public List<AlumnoDTO> getAlumnos(FiltroAlumno filtro);

}
