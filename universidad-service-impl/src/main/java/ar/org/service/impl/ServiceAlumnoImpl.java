package ar.org.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.org.commons.dto.AlumnoDTO;
import ar.org.commons.filter.base.FiltroAlumno;
import ar.org.dao.AlumnoDAO;
import ar.org.domain.model.Alumno;
import ar.org.service.ServiceAlumno;
import ar.org.service.impl.base.GenericFilterableCrudServiceImpl;
import ar.org.service.impl.dozer.DozerUtils;


@Service
@Transactional
public class ServiceAlumnoImpl extends GenericFilterableCrudServiceImpl<AlumnoDTO, Alumno, FiltroAlumno, Long> implements ServiceAlumno{

	@Autowired
	private AlumnoDAO dao;
	@Override
	public List<AlumnoDTO> getAlumnos(FiltroAlumno filtro) {
		List<Alumno> alumnos =  dao.getAlumnos(filtro);
		return alumnos == null ? null : DozerUtils.mapList(alumnos, AlumnoDTO.class);	
	}

}
