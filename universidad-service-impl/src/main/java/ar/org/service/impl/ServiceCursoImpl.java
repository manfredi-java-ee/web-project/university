package ar.org.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ar.org.commons.dto.CursoDTO;
import ar.org.commons.filter.base.FiltroBase;
import ar.org.domain.model.Curso;
import ar.org.service.ServiceCurso;
import ar.org.service.impl.base.GenericFilterableCrudServiceImpl;


@Service
@Transactional
public class ServiceCursoImpl extends GenericFilterableCrudServiceImpl<CursoDTO, Curso, FiltroBase, Long> implements ServiceCurso{

}
