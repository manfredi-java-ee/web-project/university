package ar.org.service.impl.dozer;

import java.util.List;
import java.util.Set;

import org.dozer.Mapper;

public class DozerUtils {
	
	public static Mapper getMapper() {
		return DozerManager.getInstance().getDozerMapper();
	}

	/**
	 * Helper para mapear un BusinessObject a un DTO.
	 *
	 * @param src Instancia origen desde donde se obtendran los valores.
	 * @param dest Clase del DTO destino.
	 *
	 * @return Instancia del DTO con los datos completos segun mapeos.
	 */
	public static <T> T map(Object src, Class<T> dest) {
		return (T)getMapper().map(src, dest);
	}

	/**
	 * Helper para mapear un BusinessObject a un DTO.
	 *
	 * @param src Instancia origen desde donde se obtendran los valores.
	 * @param dest Instancia de destino destino.
	 */
	public static <T> void map(Object src, Object dest) {
		getMapper().map(src, dest);
	}

	/**
	 * Helper para mapear una lista de tipos de objetos a otra lista de otro
	 * tipo de objetos.
	 *
	 * @param src Lista de instancias origen desde donde se obtendran los valores.
	 * @param dest Clase destino comun a todos los elementos de la lista.
	 *
	 * @return Lista de instancias de tipos de objetos destino con los datos completos segun mapeos.
	 */
	public static <T> List<T> mapList(List<?> src, Class<T> dest) {
		List<T> lista = CollectionFactory.createList();

		for (Object object : src) {
			lista.add(map(object, dest));
		}

		return lista;
	}
	
	public static <T> Set<T> mapSet(Set<?> src, Class<T> dest) {
		Set<T> lista = CollectionFactory.createSet();

		for (Object object : src) {
			lista.add(map(object, dest));
		}

		return lista;
	}

	/**
	 * Helper para mapear una lista de tipos de objetos a otra lista de otro
	 * tipo de objetos.
	 *
	 * @param src Lista de instancias origen desde donde se obtendran los valores.
	 * @param dest Clase destino comun a todos los elementos de la lista.
	 *
	 * @return Lista de instancias de tipos de objetos destino con los datos completos segun mapeos.
	 */
	public static <T> List<T> mapCollection(Set<?> src, Class<T> dest) {
		List<T> lista = CollectionFactory.createList();

		for (Object object : src) {
			lista.add(map(object, dest));
		}

		return lista;
	}

}
