package ar.org.service.impl.dozer;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;

public class DozerManager {
	private static DozerManager instance = null;
	private static DozerBeanMapper dozerMapper;
	
	public DozerBeanMapper getDozerMapper() {
		return dozerMapper;
	}

	private DozerManager(){
		dozerMapper = new DozerBeanMapper();
		List<String> mappingFiles = new ArrayList<String>();
		mappingFiles.add("dozer.xml");				                  
		dozerMapper.setMappingFiles(mappingFiles);
	}
	
	public static DozerManager getInstance(){
		if(instance == null){
			instance = new DozerManager();
		}
		
		return instance;
	}

}
