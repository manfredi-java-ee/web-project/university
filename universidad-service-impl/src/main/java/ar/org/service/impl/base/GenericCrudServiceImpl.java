package ar.org.service.impl.base;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.GenericTypeResolver;
import org.springframework.transaction.annotation.Transactional;

import ar.org.commons.base.DTOBase;
import ar.org.dao.GenericDAO;
import ar.org.domain.base.EntityBase;
import ar.org.service.base.GenericCrudService;
import ar.org.service.impl.dozer.DozerUtils;


@Transactional
public class GenericCrudServiceImpl <DTO extends DTOBase, ENTITY extends EntityBase, ID extends Serializable> implements GenericCrudService<DTO, ID> {
	
	@Autowired
	private GenericDAO<ENTITY, ID> dao;
		
	protected Class<DTO> dtoClass;
	protected Class<ENTITY> entityClass;

	@SuppressWarnings("unchecked")
	public GenericCrudServiceImpl() {
		dtoClass  = (Class<DTO>) GenericTypeResolver.resolveTypeArguments(getClass(), GenericCrudService.class)[0];
		entityClass = (Class<ENTITY>) GenericTypeResolver.resolveTypeArguments(getClass(), GenericCrudServiceImpl.class)[1];
	}
	
	@Override
	public DTO getById(ID id) {
		ENTITY entity = dao.find(id);
		return entity != null ? DozerUtils.map(entity, dtoClass) : null;
	}

	@Override
	public DTO save(DTO dto) {
		ENTITY entity = DozerUtils.map(dto, entityClass);
		return DozerUtils.map(dao.save(entity), dtoClass);
	}
	
	@Override
	public DTO update(DTO dto) {
		ENTITY entity = DozerUtils.map(dto, entityClass);
		return DozerUtils.map(dao.update(entity), dtoClass);
	}

	@Override
	public void delete(DTO dto) {

		ENTITY entity = DozerUtils.map(dto, entityClass);
		dao.delete(entity);
	}

	@Override
	public List<DTO> getAll() {
		List<ENTITY> list = dao.getAll();
		return DozerUtils.mapList(list, dtoClass);
	}

}
