package ar.org.service.impl.base;

import java.io.Serializable;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import ar.org.commons.base.DTOBase;
import ar.org.commons.filter.base.FiltroBase;
import ar.org.dao.GenericFilterableDAO;
import ar.org.domain.base.EntityBase;
import ar.org.service.base.GenericFilterableCrudService;
import ar.org.service.impl.dozer.DozerUtils;

@Transactional
public class GenericFilterableCrudServiceImpl <DTO extends DTOBase, ENTITY extends EntityBase, FILTRO extends FiltroBase, ID extends Serializable> extends GenericCrudServiceImpl<DTO, ENTITY, ID> implements GenericFilterableCrudService <DTO, FILTRO, ID> {
	
	@Autowired
	private GenericFilterableDAO<ENTITY, FILTRO, ID> dao;

	public GenericFilterableCrudServiceImpl() {
		super();
	}
	
	@Override
	public List<DTO> getAll(FILTRO filtro) {
		List<ENTITY> list = dao.getAll(filtro);
		return DozerUtils.mapList(list, dtoClass);
	}

}
