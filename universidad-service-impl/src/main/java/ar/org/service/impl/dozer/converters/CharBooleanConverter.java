package ar.org.service.impl.dozer.converters;

import org.dozer.DozerConverter;


public class CharBooleanConverter extends DozerConverter<Character, Boolean> {

	public CharBooleanConverter() {
		super(Character.class, Boolean.class);
	}

	public Boolean convertTo(Character source, Boolean destination) {
		if (source == null)
			return null;

		return 'S' == source;
	}

	public Character convertFrom(Boolean source, Character destination) {
		if (source == null)
			return null;

		return source ? 'S' : 'N';
	}

}