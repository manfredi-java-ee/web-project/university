package ar.org.dao.impl;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import ar.org.commons.filter.base.FiltroBase;
import ar.org.dao.CursoDAO;
import ar.org.dao.base.GenericFilterableDAOHibernateImpl;
import ar.org.domain.model.Curso;

@Repository
public class HibernateCursoDAO extends GenericFilterableDAOHibernateImpl<Curso, FiltroBase, Long> implements CursoDAO  {

	@Override
	protected void addFilters(FiltroBase filtro, Criteria criteria) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void createAlias(Criteria criteria) {
		// TODO Auto-generated method stub
		
	}

}
