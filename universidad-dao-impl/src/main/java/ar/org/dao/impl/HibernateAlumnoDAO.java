package ar.org.dao.impl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import ar.org.commons.filter.base.FiltroAlumno;
import ar.org.dao.AlumnoDAO;
import ar.org.dao.base.GenericFilterableDAOHibernateImpl;
import ar.org.domain.model.Alumno;

@Repository
public class HibernateAlumnoDAO extends GenericFilterableDAOHibernateImpl<Alumno, FiltroAlumno, Long> implements AlumnoDAO  {


	@Override
	public List<Alumno> getAlumnos(FiltroAlumno filtro) {
		Criteria criteria = getCriteria();
		addFilters(filtro, criteria);
		return list(criteria);
	}
	
	@Override
	protected void addFilters(FiltroAlumno filtro, Criteria criteria) {
		if(filtro == null)
			return;
		
		if(filtro.getIds().length >0 ) {
			criteria.add(Restrictions.not(Restrictions.in("id", filtro.getIds())));	
		}
		
	}
	

	@Override
	protected void createAlias(Criteria criteria) {
	}
}
