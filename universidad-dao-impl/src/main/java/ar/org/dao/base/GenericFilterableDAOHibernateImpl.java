package ar.org.dao.base;

import java.io.Serializable;
import java.util.List;

import org.hibernate.Criteria;

import ar.org.commons.filter.base.FiltroBase;
import ar.org.dao.GenericFilterableDAO;
import ar.org.domain.base.EntityBase;


public abstract class GenericFilterableDAOHibernateImpl<ENTITY extends EntityBase, FILTER extends FiltroBase, ID extends Serializable> extends GenericDAOHibernateImpl<ENTITY, ID> implements GenericFilterableDAO<ENTITY, FILTER, ID> {

	public List<ENTITY> getAll(FILTER filtro) {
		Criteria criteria = getCriteria();
		createAlias(criteria);
		addFilters(filtro, criteria);
		return list(criteria);
	}

	protected abstract void addFilters(FILTER filtro, Criteria criteria);
	
	protected abstract void createAlias(Criteria criteria);	

}
