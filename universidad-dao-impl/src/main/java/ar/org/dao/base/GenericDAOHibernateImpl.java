package ar.org.dao.base;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;

import ar.org.dao.GenericDAO;
import ar.org.domain.base.EntityBase;


public abstract class GenericDAOHibernateImpl<ENTITY extends EntityBase, ID extends Serializable> implements GenericDAO<ENTITY, ID> {
	
	private Class<ENTITY> type;
	
	@Autowired
	private SessionFactory sessionFactory;
		
	@SuppressWarnings("unchecked")
	public GenericDAOHibernateImpl() {					
		type = (Class<ENTITY>)((ParameterizedType)getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	public Session getSession() {
		return sessionFactory.getCurrentSession();
	}
	
	public Criteria getCriteria() {
		return getSession().createCriteria(type);
	}
	
	@SuppressWarnings("unchecked")
	public ENTITY create(ENTITY entity) {
		return (ENTITY) getSession().save(entity);
	}

	@SuppressWarnings("unchecked")		
	public ENTITY find(ID id) {		
		return (ENTITY) getSession().get(type, id);
	}

	public ENTITY update(ENTITY entity) {
		getSession().merge(entity);
		return entity;
	}
	
	@SuppressWarnings("unchecked")
	public ENTITY save(ENTITY entity) {
		return (ENTITY) getSession().merge(	entity);
	}

	public void delete(ENTITY entity) {
		getSession().delete(getSession().contains(type) ? type : getSession().merge(type));
	}
	
	public List<ENTITY> getAll(){
		Criteria criteria = getSession().createCriteria(type);
		return list(criteria);
	}
	
	public int getAllCount() {
		Criteria criteria = getSession().createCriteria(type);
		criteria.setProjection(Projections.rowCount());
		return uniqueIntegerResult(criteria);
	}
	
	@SuppressWarnings("unchecked")
	protected List<ENTITY> list(final Criteria criteria) {
		return (List<ENTITY>) criteria.list();
	}
	
	@SuppressWarnings("unchecked")
	protected List<ENTITY> list(final Query query) {
		return (List<ENTITY>) query.list();
	}
	
	@SuppressWarnings("unchecked")
	protected ENTITY uniqueResult(final Criteria criteria) {
		return (ENTITY) criteria.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	protected ENTITY uniqueResult(final Query query) {
		return (ENTITY) query.uniqueResult();
	}
	
	protected Integer uniqueIntegerResult(final Criteria criteria) {
		return ((Number) criteria.uniqueResult()).intValue();
	}
	
	protected Number uniqueNumberResult(final Criteria criteria) {
		return (Number) criteria.uniqueResult();
	}
	
}
